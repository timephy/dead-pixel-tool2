const div = () => document.createElement("div");
const span = () => document.createElement("span");

const camContainer = document.getElementById("camera-container");

class Bar {
    constructor(pixels) {
        this.div = div();

        this.span = span();
        this.div.appendChild(this.span);

        this.amount = 1;
        this.isCritical = void 0;
        this.label = "LABELLLLL;"

        this.html = this.div;
        this.basePixels = pixels;
    }

    set pixels(pixels) {
        this.filteredPixels = pixels;
        this.amount = pixels.length;
        this.label =
            "Pixels in this group (x:y:value):\n" +
            pixels.map((p) => `${p.x}:${p.y}:${p.value}`).join(", ");
        //pixels.map((p) => `(${p.x}:${p.y}:${p.value})`).join(", ");
    }

    set label(text) {
        this.div.title = text;
    }

    set amount(num) {
        this.span.innerHTML = num;
        this.div.style.width = `${num * 4 + 10 - 4}px`;
        this.div.style.display = num ? "" : "none";
    }

    set isCritical(bool) {
        if (bool === void 0) this.div.classList = void 0;
        else this.div.classList = bool ? "critical" : "non-critical";
    }
}

class Bars {
    constructor(bars) {
        this.div = div();
        this.div.classList = "bars";

        this.bars = bars.map((bar) => new Bar(bar));
        this.bars.forEach((bar) => this.div.appendChild(bar.html));

        this.html = this.div;
    }
}

class Image {
    constructor(imageObj) {
        this.div = div();

        this.span = span();
        this.span.innerHTML = imageObj.id.split("-")[1];
        this.isBrightImage = imageObj.isBrightImage;
        this.div.appendChild(this.span);

        if (this.isBrightImage) {
            this.div.classList = "brightImage";
        }

        this.width = imageObj.width;
        this.height = imageObj.height;
        this.bars = new Bars(imageObj.deadPixelGroups);

        this.div.appendChild(this.bars.html);

        this.html = this.div;
    }
}

class Camera {
    constructor(id, imageObjs) {
        this.div = div();

        this.span = span();

        this.img = document.createElement("img");
        this.img.classList = "close-camera";
        this.img.src = ".visual/images/icon-close-128.png";

        this.imagesDiv = div();
        this.imagesDiv.classList = "images";

        this.images = imageObjs.map((image) => new Image(image));

        this.id = id;

        this.images.forEach((image) => this.imagesDiv.appendChild(image.html));

        this.div.appendChild(this.span);
        this.div.appendChild(this.img);
        this.div.appendChild(this.imagesDiv);

        this.html = this.div;

        camContainer.appendChild(this.html)
    }

    set id(id) {
        this.span.innerHTML = id;
    }

    remove() {
        camContainer.removeChild(this.div);
    }
}
