function removeCam(camId) {
    cams[camId].remove();
    delete cams[camId];
}

const camDataMap = {};
for (const img of imageData) {
    const camId = img.id.split("-")[0];
    if (!camDataMap[camId]) camDataMap[camId] = [];
    camDataMap[camId].push(img);
}

const cams = {};
for (const camId in camDataMap) {
    cams[camId] = new Camera(camId, camDataMap[camId]);
}

document.addEventListener("click", function (e) {
    if (e.target.classList[0] === "close-camera") {
        const camId = e.target.parentElement.firstChild.innerText;
        console.log(`close '${camId}'`);
        removeCam(camId);
    }
});

let IS_ELLIPSE = true;
let THRESHOLD = 10;
let X_INSET = 300;
let Y_INSET = 300;

document.addEventListener("change", function (e) {
    const target = e.target;
    const id = target.id;
    if (id === "ellipse") {
        IS_ELLIPSE = target.checked;

    } else if (id === "threshold") {
        if (target.value > 255) {
            e.target.value = 255;
        } else if (target.value < 10 || target.value === "") {
            target.value = 10;
        }
        THRESHOLD = Number.parseInt(target.value);

    } else if (id === "x-inset") {
        if (target.value < 0 || target.value === "") {
            target.value = 0;
        }
        X_INSET = Number.parseInt(target.value);

    } else if (id === "y-inset") {
        if (target.value < 0 || target.value === "") {
            target.value = 0;
        }
        Y_INSET = Number.parseInt(target.value);

    }
    update();
});

function update() {
    // calculate number
    for (camId of Object.keys(cams)) {
        const cam = cams[camId];
        for (const image of cam.images) {
            for (const bar of image.bars.bars) {
                if (image.isBrightImage) {
                    bar.pixels = bar.basePixels;
                } else {
                    bar.pixels = bar.basePixels.filter((p) => p.value >= THRESHOLD);
                }
            }
        }
    }
    // set if critical
    for (camId of Object.keys(cams)) {
        const cam = cams[camId];
        for (const image of cam.images) {
            setBarPixelGroupIsCritical(image);
        }
    }
}

function setBarPixelGroupIsCritical(image) {
    const { width, height } = image;
    const selectionWidth = width - 2 * X_INSET;
    const selectionHeight = height - 2 * Y_INSET;
    const xInsetInverse = width - X_INSET;
    const yInsetInverse = height - Y_INSET;

    const select = IS_ELLIPSE ?
        (pixel) => {
            const { x, y } = pixel;
            // 1D-dist to center
            const xMidDist = Math.abs(width / 2 - x);
            const yMidDist = Math.abs(height / 2 - y);
            // Between 0 and 1 if in ellipse, making it a circle
            const xMidDistNormal = xMidDist / (selectionWidth / 2);
            const yMidDistNormal = yMidDist / (selectionHeight / 2);
            // Pythagorean formula to get 2D-dist to center
            // BUT: no need for 'sqrt', therefore better performance
            const dist = xMidDistNormal ** 2 + yMidDistNormal ** 2;
            // const dist = Math.sqrt(xMidDistNormal ** 2 + yMidDistNormal ** 2);
            // returns 'true' if pixel is in ellipse
            return dist <= 1;
        } :
        (pixel) => {
            const { x, y } = pixel;
            return (
                x >= X_INSET &&
                x <= xInsetInverse &&
                y >= Y_INSET &&
                y <= yInsetInverse
            );
        };
    for (bar of image.bars.bars) {
        bar.isCritical = bar.filteredPixels.some(select);
    }
}

update();
