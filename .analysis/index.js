const fs = require("fs");
const fork = require("child_process").fork;
const NUM_THREADS = require("os").cpus().length; // - 1;

const processes = [];
const data = [];

const config = require("../config.json");

const dataDir = "./data/"
const imagesDir = "./images/";

/* ============================================================ */

var imageFileNames;
try {
    imageFileNames = fs.readdirSync(imagesDir);
    console.log(imageFileNames)
    //imageFileNames = imageFileNames.filter(name => name.split("-")[1][0] !== "B");
} catch (e) {
    console.log("Folder 'images' did not exist, so it was created for you. Put in images and re-run.")
    fs.mkdirSync("images")
    process.exit(1);
}
const countImages = imageFileNames.length;

if (!countImages) {
    console.log("No images found in folder 'images'.");
    process.exit(1);
}

/* ============================================================ */

const end = () => {
    for (const child of processes) {
        child.kill();
    }
    const outData = data.sort(function (a, b) {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return 1;
        return 0;
    });
    if (!fs.existsSync(dataDir)) fs.mkdirSync(dataDir);
    // write raw data to js file for inspection
    fs.writeFileSync(".visual/" + "dead-pixels-data.js", "const imageData = " + JSON.stringify(outData, null, 2));
    /* ============================ */
    // filter data for wanted output for json and csv
    /* JSON */
    for (const img of outData) {
        if (img.isBrightImage) {
            img.deadPixelGroups = img.deadPixelGroups.map(arr => arr.filter(
                (pxl) => pxl.value <= config.brightImageThreshold
            ));
        } else {
            img.deadPixelGroups = img.deadPixelGroups.map(arr => arr.filter(
                (pxl) => pxl.value >= config.threshold
            ));
        }
    }
    const jsonOut = {
        config: config,
        images: outData
    }
    fs.writeFileSync(dataDir + "dead-pixels.json", JSON.stringify(jsonOut, null, 2));
    /* CSV */
    // filter and format data for csv
    csvData = ["IMAGE_ID,DEAD_PIXELS(x:y:value:failure)..."];
    csvData.push([
        `THRESHOLD=${config.threshold},BRIGHT_IMAGE_THRESHOLD=${config.brightImageThreshold}`
    ]);
    for (const img of outData) {
        const line = [img.id];
        for (const group of img.deadPixelGroups) {
            line.push(...group.map((p) => `${p.x}:${p.y}:${p.value}:${""}`));
        }
        csvData.push(line.join(","));
    }
    fs.writeFileSync(dataDir + "dead-pixels.csv", csvData.join("\n"));
    console.log("##### DONE #####")
};

const assignProcess = (proc) => {
    const newImageName = imageFileNames.pop();
    if (newImageName) {
        proc.send(imagesDir + newImageName, function () { /**/ });
    } else if (data.length === countImages) {
        end();
    }
}

const processAnalysisComplete = (proc, result) => {
    data.push(result);
    assignProcess(proc);
}

console.log(`Creating ${NUM_THREADS} processes...`);
for (let i = 0; i < NUM_THREADS; i++) {
    const child = processes[i] = fork("./.analysis/analyze-jpeg.js");
    child.on("message", (result) => processAnalysisComplete(child, result));
}
console.log("Created Processes.");

for (proc of processes) {
    assignProcess(proc);
}
