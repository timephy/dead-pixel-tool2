const JPEG = require("./jpeg");
const fs = require("fs");

const VALUE_THRESHOLD = require("../config.json").threshold;
const VALUE_THRESHOLD_BRIGHT = require("../config.json").brightImageThreshold;

function loadImage(path) {
    const file = fs.readFileSync(path);
    return JPEG(file, true);
}

function getDeadPixels(image, isBrightImage) {
    const deadPixels = [];
    let i = 0;
    while (i < image.data.length) {
        const data = image.data;
        const r = data[i + 0];
        const g = data[i + 1];
        const b = data[i + 2];

        let selected = false;
        let value = Number.NAN;
        if (isBrightImage) {
            value = r;
            value = g < value ? g : value;
            value = b < value ? b : value;
            selected = value <= VALUE_THRESHOLD_BRIGHT;
        } else {
            value = r;
            value = g > value ? g : value;
            value = b > value ? b : value;
            selected = value >= VALUE_THRESHOLD;
        }

        if (selected) {
            const obj = {
                x: Math.floor(i / 4) % image.width + 1,
                y: Math.floor(i / 4 / image.width) + 1,
                value
            };
            deadPixels.push(obj);
        }
        //
        i += 4;
    }
    return deadPixels;
}

// function pixelsAreTouching(p1, p2) {
//     return (
//         p1.x === p2.x && p1.y === p2.y + 1 ||
//         p1.x === p2.x && p1.y === p2.y - 1 ||
//         p1.x === p2.x + 1 && p1.y === p2.y ||
//         p1.x === p2.x - 1 && p1.y === p2.y ||
//         p1.x === p2.x + 1 && p1.y === p2.y + 1 ||
//         p1.x === p2.x - 1 && p1.y === p2.y + 1 ||
//         p1.x === p2.x + 1 && p1.y === p2.y - 1 ||
//         p1.x === p2.x - 1 && p1.y === p2.y - 1
//     );
// }

function pixelsHaveMaxDist(p1, p2, d) {
    return (Math.abs(p1.x - p2.x) <= d && Math.abs(p1.y - p2.y) <= d);
}

function groupDeadPixels(pxls) {
    let pixels = pxls;
    let length = pixels.length;
    const groups = [];
    while (length) {
        const pxl = pixels.shift();
        const group = [pxl];
        groups.push(group);
        const arr = [];
        for (const p of pixels) {
            if (group.some((pxl) => pixelsHaveMaxDist(pxl, p, 10))) {
                group.push(p);
            } else {
                arr.push(p);
            }
        }
        pixels = arr;
        length = pixels.length;
    }
    return groups;
}

process.on("message", function (imageFilePath) {
    console.log(`Process ${process.pid} now analyzing: ${imageFilePath}`);
    const imageFilePathParts = imageFilePath.split("/");
    const imageFileName = imageFilePathParts[imageFilePathParts.length - 1].split(".")[0];

    const image = loadImage(imageFilePath);
    const isBrightImage = imageFileName.includes("B");

    const deadPixels = getDeadPixels(image, isBrightImage);
    const groupedDeadPixels = groupDeadPixels(deadPixels);

    process.send({
        id: imageFileName,
        isBrightImage,
        width: image.width,
        height: image.height,
        deadPixelGroups: groupedDeadPixels
    })
});
